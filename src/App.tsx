import * as React from 'react';
import './App.css';
import Demo001 from "./components/Demo001";
import Header from "./common/Header";

//Master commit latest update
class App extends React.Component {
    public render() {
        return (
            <div className="App">
                <Header/>
                <div className="demos">
                    Demo Applications Container
                    <Demo001/>
                </div>
            </div>
        );
    }
}

export default App;
