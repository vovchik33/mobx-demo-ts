import * as React from 'react';
import {Component} from 'react';

class Header extends Component {
    render() {
        return (
            <header className="app-header">
                <h1 className="app-title">Header</h1>
            </header>
        );
    }
}

export default Header;