import {autorun, computed, observable} from "mobx";
import './index.css';
import ToDoItem from "./ToDoItem";


class ToDoStore {
    @observable public todos = [new ToDoItem(), new ToDoItem()];
    @computed get handledTodos() {
        return this.todos.slice();
    }
    public addTodo(item:string) {
        console.log("Add New Item: "+JSON.stringify(item));
        this.todos.push(new ToDoItem());
    }
}

// @ts-ignore
export const store = new ToDoStore();

autorun(()=> {
    console.log(JSON.stringify(store.todos));
}, {delay: 100});