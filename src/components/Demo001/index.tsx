import * as React from 'react';
// @ts-ignore
import {observer} from "mobx-react";
import {store} from './index.store';

@observer
class Demo001 extends React.Component {
    public clickAddHandler() {
        console.log("CLICKED");
        store.addTodo("SAD");
    }
    public render() {
        const todosJSX = store.handledTodos.map((item)=>{
            return <li key={item.id}>{item.value}</li>
        })
        return (
            <div>
                <h3>Demo_001 Simple Mobx App</h3>
                <div className="todos-controls">
                    <input type="text"/>
                    <button onClick={this.clickAddHandler}>Add Task</button>
                </div>
                <div className="todos-container">
                    <ul>
                        {todosJSX}
                    </ul>
                </div>
            </div>
        );
    }
}


export default Demo001;